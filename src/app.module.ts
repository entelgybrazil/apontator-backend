import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
// import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { JobsModule } from './jobs/jobs.module';

@Module({
  imports: [TypeOrmModule.forRoot(typeOrmConfig), UsersModule, JobsModule],
  controllers: [],
  providers: [],
})
export class AppModule {}