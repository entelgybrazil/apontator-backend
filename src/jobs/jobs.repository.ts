import { Job } from "./jobs.entity";
import { Repository, EntityRepository } from "typeorm";
import { CreateJobDto } from "./dto/create-job.dto";
import { InternalServerErrorException } from "@nestjs/common";

@EntityRepository(Job)
export class JobsRepository extends Repository<Job> {

    async createJob(createJobDto: CreateJobDto): Promise<Job> {
        const { title } = createJobDto;

        const job = this.create();
        job.title = title.toUpperCase();

        try {
            await job.save();
            return job;
        } catch (error) {
            throw new InternalServerErrorException(
            'Erro ao salvar o cargo no banco de dados',
            error.message
            );
        }
    }

}