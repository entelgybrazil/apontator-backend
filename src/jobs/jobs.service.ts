import { Injectable } from '@nestjs/common';
import { CreateJobDto } from './dto/create-job.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { JobsRepository } from './jobs.repository';

@Injectable()
export class JobsService {

    constructor(
        @InjectRepository(JobsRepository)
        private jobsRepository: JobsRepository
    ){}

    async createJob(createJobDto: CreateJobDto) {
        return this.jobsRepository.createJob(createJobDto);
    }
}
