import { Controller, Post, Body } from '@nestjs/common';
import { JobsService } from './jobs.service';
import { CreateJobDto } from './dto/create-job.dto';

@Controller('jobs')
export class JobsController {

    constructor(private jobsService: JobsService){}

    @Post('create')
    async createUser(
        @Body() createJobDto: CreateJobDto,
    ): Promise<any> {
        const job = await this.jobsService.createJob(createJobDto);
        return {
        job,
        message: 'Cargo cadastrado com sucesso',
        };
    }
}
