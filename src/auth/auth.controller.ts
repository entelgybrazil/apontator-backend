// /* eslint-disable @typescript-eslint/explicit-module-boundary-types */
// import { Controller, Get, UseGuards, Res, Req, Post } from '@nestjs/common';
// import { AuthGuard } from '@nestjs/passport';
// import { AuthService } from './auth.service';

// @Controller('auth')
// export class AuthController {

//     constructor(private authService: AuthService){}

//     @Post('google')
//     googleAuth(@Req() request){
//         return this.authService.validateOAuthLogin(request);
//     }
    
//     @Get('google')
//     @UseGuards(AuthGuard('google'))
//     googleLogin(){
//         // initiates the Google OAuth2 login flow
//     }

//     @Get('google/callback')
//     @UseGuards(AuthGuard('google'))
//     googleLoginCallback(@Req() req, @Res() res){
//         const jwt: string = req.user.jwt;
//         if (jwt)
//             res.redirect('http://localhost:4200/login/succes/' + jwt);
//         else 
//             res.redirect('http://localhost:4200/login/failure');
//     }

//     @Get('protected')
//     @UseGuards(AuthGuard('jwt'))
//     protectedResource()
//     {
//         return 'JWT is working!';
//     }

// }
