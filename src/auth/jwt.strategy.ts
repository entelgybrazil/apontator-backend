// /* eslint-disable @typescript-eslint/explicit-module-boundary-types */
// /* eslint-disable @typescript-eslint/ban-types */
// import { Injectable, UnauthorizedException } from '@nestjs/common';
// import { PassportStrategy } from '@nestjs/passport';
// import { ExtractJwt, Strategy } from 'passport-jwt';
// import { AuthService } from './auth.service';

// @Injectable()
// export class JwtStrategy extends PassportStrategy(Strategy, 'jwt')
// {

//     constructor(private readonly authService: AuthService) 
//     {
//         super({
//             jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
//             secretOrKey: 'QZykADM3EkY+i8DxXCej/hu1q950qgXXRcu3JzMBqzj3PaByd34VE8sOoXpQbBDPTyZeQgGT6BDW+IXOsd89zDgQY/ZR6v1exR1rO3NBS+3QHjoT7IfA6dJiLJLmLOYDR4O5ue1iaTheqiUqaLm58H74wZiWAnexelx5BQM/Fjyv1qvxKX6kECLfl7DtGlZJJa9eoXCtlBIqzkYfmTQsBLyzU5LoCa+I5mtjO3LeYSF7OrGIDsKBv7B0UerRHfpPHCqbiqxLaQyqNaiUezaeryMW5nDTn2noaMvJy4taUo2sRp8UuxJKfwTjOKJkfbV6qHZbc2+MgVpBCMl4Uf1Ksg=='
//         });
//     }

//     async validate(payload: any, done: Function)
//     {
//         try
//         {
//             // You could add a function to the authService to verify the claims of the token:
//             // i.e. does the user still have the roles that are claimed by the token
//             //const validClaims = await this.authService.verifyTokenClaims(payload);
            
//             //if (!validClaims)
//             //    return done(new UnauthorizedException('invalid token claims'), false);
    
//             done(null, payload);
//         }
//         catch (err)
//         {
//             throw new UnauthorizedException('unauthorized', err.message);
//         }
//     }

// }