// import { Module } from '@nestjs/common';
// import { AuthController } from './auth.controller';
// import { AuthService } from './auth.service';
// // import { GoogleStrategy } from './google.strategy';
// import { JwtStrategy } from './jwt.strategy';
// import { TypeOrmModule } from '@nestjs/typeorm';
// import { UserRepository } from 'src/users/users.repository';
// import { JwtModule } from '@nestjs/jwt';
// import { UsersService } from 'src/users/users.service';
// import { UsersModule } from 'src/users/users.module';


// @Module({
//   controllers: [AuthController],
//   providers: [
//     AuthService,
//     UsersService,
//     // GoogleStrategy,
//     JwtStrategy
//   ],
//   imports: [
//     UsersModule,
//     TypeOrmModule.forFeature([UserRepository]),
//     JwtModule.register({
//       secret: 'super-secret',
//       signOptions: {
//         expiresIn: 18000,
//       },
//     }),
//   ]
// })
// export class AuthModule {}
