/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Injectable, UnprocessableEntityException} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from './users.repository';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './users.entity';
import { UserRole } from './enum/user-roles.enum';
// import { Provider } from '../auth/auth.service';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ) {}

  async createUser(createUserDto: CreateUserDto, role: UserRole): Promise<User> {
    return this.userRepository.createUser(createUserDto, role);
  }

  async findUserById(id: string): Promise<User> {
    return this.userRepository.findUserById(id);
  }

  async findAll(): Promise<User[]> {
    return this.userRepository.findAll();
  }

  async updateUser(user: User): Promise<User> {
    return this.userRepository.updateUser(user);
  }

  async deleteUserById(id: string): Promise<any> {
    return this.userRepository.deleteUserById(id);
  }

  // async registerOAuthUser(createUserGoogleDto: CreateUserGoogleDto): Promise<User> {
  //   return this.userRepository.createUser(createUserGoogleDto);
  // }

  // findOneByGoogleId(id: string): Promise<User> {
  //   return this.userRepository.findOne({id})
  // }

}