import {
    BaseEntity,
    Entity,
    Unique,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    JoinColumn,
    ManyToOne,
  } from 'typeorm';
import { Job } from 'src/jobs/jobs.entity';
  
  @Entity()
  @Unique(['email'])
  @Unique(['employeeId'])
  export class User extends BaseEntity {
    
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({ nullable: false, type: 'varchar', length: 200 })
    email: string;
  
    @Column({ nullable: false, type: 'varchar', length: 200 })
    name: string;

    @Column({ nullable: true })
    birthday: Date
  
    @Column({ nullable: false, type: 'varchar', length: 20 })
    role: string;
  
    @Column({ nullable: false, default: true })
    status: boolean;

    @Column({ nullable: true})
    locale: string

    @Column({ nullable: false, type: 'varchar', length: 10, comment: 'Matrícula do usuário'})
    employeeId: string

    @Column({ nullable: true, comment: 'Data de início'})
    admissionDt: Date

    @Column({ nullable: true, comment: 'Data de desligamento'})
    terminationDt: Date

    @Column({ nullable: false, comment: 'Tipo do contrato'})
    typeContract: string

    @Column({ default: false })
    isManager: boolean

    @Column({ nullable: true, comment: 'Departamento do usuário'})
    department: string

    @CreateDateColumn()
    createdAt: Date;
  
    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne((type) => Job, {
      cascade: false
    })
    @JoinColumn()
    job: Job
  }