/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Controller, Get, Post, Body, Param, Patch, Req, Delete } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { ReturnUserDto } from './dto/return-user.dto';
import { UserRole } from './enum/user-roles.enum';
import { User } from './users.entity';

@Controller('users')
export class UsersController {

    constructor(private usersService: UsersService){}

    @Post('create')
    async createUser(
        @Body() createUserDto: CreateUserDto,
    ): Promise<ReturnUserDto> {
        let user: any

        if(createUserDto.role.toUpperCase() === UserRole.ADMIN){
            user = await this.usersService.createUser(createUserDto, UserRole.ADMIN);
        }else{
            user = await this.usersService.createUser(createUserDto, UserRole.USER);
        }

        return {
        user,
        message: 'User cadastrado com sucesso',
        };
    }

    @Get('all')
    async findAll(): Promise<User[]>{ 
        return this.usersService.findAll();
    }

    @Get(":id")
    async findUserById(@Param() params): Promise<User>{ 
        return this.usersService.findUserById(params.id);
    }

    @Patch(":id")
    async updateUserById(@Req() req): Promise<User> {
        return this.usersService.updateUser(req.body);
    }

    @Delete(":id")
    async deleteUserById(@Param() params): Promise<any> {
        return this.usersService.deleteUserById(params.id)
    }
}
