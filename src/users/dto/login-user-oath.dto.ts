export class LoginUserGoogleDto {
    sub: string;
    name: string;
    given_name: string;
    family_name: string;
    picture: 'https://lh3.googleusercontent.com/a-/AOh14Gg8opk2SZKuK2QVMzySCMNxISn7TeFajggWUEPq';
    email: string
    locale: string;
    hd: 'entelgy.com';
}