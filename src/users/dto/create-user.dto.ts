import { Job } from "src/jobs/jobs.entity";

export class CreateUserDto {
  email: string;
  name: string;
  employeeId: string;
  admissionDt: Date;
  typeContract: string;
  department: string;
  role: string;
  job: Job;
}