import { EntityRepository, Repository } from 'typeorm';
import { User } from './users.entity';
import { UserRole } from './enum/user-roles.enum';
import {
  ConflictException,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';

@EntityRepository(User)
export class UserRepository extends Repository<User> {

  async createUser(
    createUserDto: CreateUserDto,
    role: UserRole
  ): Promise<User> {
    const { email, name, employeeId, admissionDt, typeContract, department, job} = createUserDto;

    const user = this.create();
    user.email = email;
    user.name = name;
    user.status = true;
    user.employeeId = employeeId
    user.admissionDt = admissionDt
    user.typeContract = typeContract
    user.department = department
    user.role = role
    user.job = job

    try {
      await this.save(user);
      return user;
    } catch (error) {
      if (error.code.toString() === '23505') {
        throw new ConflictException('Endereço de email ou matrícula já estão em uso');
      } else {
        throw new InternalServerErrorException(
          'Erro ao salvar o usuário no banco de dados',
          error.message
        );
      }
    }
  }

  async findAll(): Promise<User[]> {

    const users = await this.find();
    if (!users) throw new NotFoundException('Não existem usuários cadastrados.');

    return users;
  }

  async findUserById(id: string): Promise<User> {

    const user = await this.findOne(id);
    if (!user) throw new NotFoundException('Usuário não encontrado');

    return user; 
  }

  async updateUser(user: User): Promise<User> {

    try{
      return await this.save(user);
    } catch (error){
      throw new InternalServerErrorException(
        'Erro ao atualizar o usuário no banco de dados',
        error.message
      );
    }
  }

  async deleteUserById(id: string): Promise<any>{

    try{
      await this.delete(id);

      return {
        'code': 200,
        'message': 'Usuário deletado.'
      }
    } catch (error){
      throw new InternalServerErrorException(
        'Erro ao deletar o usuário no banco de dados',
        error.message
      );
    }
    

  }

}